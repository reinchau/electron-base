# Electron base library

Provides abstract base app class and window class for other Electron app to inherit.

## INSTALLATION

1. Install `node-gyp` globally: `npm install --global node-gyp`

1. Install `node-pre-gyp` globally: `npm install --global node-pre-gyp`

1. Install `windows-build-tools` globally: `npm install --global --production windows-build-tools`

## DEVELOP

1. `npm install`

1. `gulp watch`

## RELEASE

* `gulp release`